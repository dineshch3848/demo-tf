provider "google" {
  version = "3.25.0"
  credentials = "./creds/serviceaccount.json"
  project = "terraform-poc-279707"
  region  = "asia-south1"
  zone    = "asia-south1-a"
}
// Create Dev-VPC
resource "google_compute_network" "vpcdev" {
 name                    = "dev-vpc"
 auto_create_subnetworks = "false"
}

